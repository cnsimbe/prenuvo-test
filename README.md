# Prenuvo Code Challenge

The code explanation can be found [here](https://docs.google.com/document/d/1nJGlGheIn5b1pA-0JpoFK3IkIyFo0k2J6hzTFmCwJ3s/edit?usp=sharing)

## Prerequisites

This project requires NPM and Ionic 4

On Mac/Linux, run the following command to install Ionic 4

```
sudo npm i -g ionic
```


## Installing and Running

Clone the repository

```
git clone https://gitlab.com/cnsimbe/prenuvo-test.git
```


And then install the node modules, and run the web application

```
cd prenuvo-test
npm install
ionic serve
```


### Using the Application

###### Image Grid
* On the Image Grid page, upload a folder of images to view the results. In this case, upload the folder ‘862625ef’ in code_challenge -> 1. HTML5 folder-uploads recursively go through a folder and its subfolders. For Safari, select multi files to upload instead


## Built With

* [Angular](https://angular.io/) - Angular v6
* [Ionic](http://ionicframework.com/) - Cross platform & progressive web application framework