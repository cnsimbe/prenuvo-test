import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'patient-form',
    pathMatch: 'full'
  },
  {
    path: 'patient-form',
    loadChildren: './patient-form/patient-form.module#PatientFormModule'
  },
  {
    path: 'image-grid',
    loadChildren: './list/list.module#ListPageModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
