import { Component, OnInit } from '@angular/core';
import { Question, Section, QuestionDataService } from "../services/question-data.service"
import { NgForm } from "@angular/forms"
import { AlertController } from "@ionic/angular"

@Component({
  selector: 'app-patient-form',
  templateUrl: 'patient-form.page.html',
  styleUrls: ['patient-form.page.scss'],
})
export class PatientForm implements OnInit {

	sections:Section[];

	constructor(private formQuestions:QuestionDataService, private alertCtrl:AlertController){}

	async ngOnInit() {
		this.sections = await this.formQuestions.getQuestions();
	}

	async saveForm() {
		for(let section of this.sections)
		{
			let result = this.isSectionValid(section);
			if(!result.valid)
				return (await this.alertCtrl.create({header:"Incomplete Form",subHeader:`${result.section.title}`,message:`Question: <i>${result.question.description}</i>`})).present()
		}

		(await this.alertCtrl.create({header:"Form Complete", subHeader:"Data successfully entered"})).present();
	}

	private isSectionValid(section:Section):{valid:boolean,section:Section,question:Question} {
		
		let result = { valid:true,section:section,question:undefined };
		if(section.enabled==false)
			return result;
		for(let question of section.questions)
		{
			if(question.required==true && question.values.length==0)
			{
				result.question = question;
				result.valid = false
				return result;
			}
			for(let subSection of question.subSections)	
			{
				let result = this.isSectionValid(subSection);
				if(result.valid==false)
					return result;
			}
		}
		return result;	
	}

	inputChanged($event:CustomEvent,question:Question,isRadio=false){
		let option = question.options.find(opt=>opt.value==$event.detail.value);
		option.selected = $event.detail.checked;

		if(isRadio==true)
			question.options.forEach(opt=>opt.selected = opt==option ? opt.selected : false);

		question.values = question.options.filter(a=>a.selected==true).map(a=>a.value);
		for(let value in question.subSectionMatch){
			let section = question.subSections.find(s=>s.id==question.subSectionMatch[value]);
			section.enabled = question.values.indexOf(value)>=0;
		}
	}



}