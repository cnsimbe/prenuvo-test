import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class OptimizeVisualPlacementService {


  public optimizeView<T>(values:Array<T>,numCols:number):ImageColumn<T>[] {
    let columns = new Array<ImageColumn<T>>()
    for(let i=0;i<numCols;i++)
      columns.push(ImageColumn.create());

    values.forEach(a=>columns[0].push(a));
    columns[0].update();

    for(let k=0;k<numCols;k++)
      for(let i=0;i<numCols-1;i++)
        for(let j=i+1;j<numCols;j++)
          this.optimizeColumns([ columns[i], columns[j] ]);
    
    return columns;
  }


  private optimizeColumns<T>(columns:ImageColumn<T>[]) {

    let lowestColumnIndex = this.getLowestColIndex(columns);
    let lowColumn = columns[lowestColumnIndex];
    let currentDebt = this.calcVisualDebt(columns);

    if(currentDebt==0)
      return columns;

    let minimizedDebt = currentDebt;
    let minimizedColIndex = undefined;
    let minimizedValueIndex = undefined;

    for(let i=0;i<columns.length;i++)
    {
      let col = columns[i];
      if(col.sum>lowColumn.sum)
      {
        for(let j=0;j<col.length;j++) {
          let debtValue = this.calFutureVisualDebt(columns,lowestColumnIndex,i,j);
          if(debtValue<minimizedDebt)
          {
            minimizedDebt = debtValue;
            minimizedColIndex = i;
            minimizedValueIndex = j;
          }
        }
      }
    }

    if(minimizedDebt<currentDebt)
    {
      this.rerrange(columns,lowestColumnIndex,minimizedColIndex,minimizedValueIndex);
      return this.optimizeColumns(columns);
    }
    else
      return columns;
  }

  private getLowestColIndex<T>(columns:ImageColumn<T>[]):number {
    let index = 0;
    let value = columns[index].sum;

    for(let i=0;i<columns.length;i++)
    {
      if(columns[i].sum<value)
      {
        value = columns[i].sum;
        index = i;
      }
    }
    return index;
  }

  private getHighestColIndex<T>(columns:ImageColumn<T>[]):number {
    let index = 0;
    let value = columns[index].sum;

    for(let i=0;i<columns.length;i++)
    {
      if(columns[i].sum>value)
      {
        value = columns[i].sum;
        index = i;
      }
    }
    return index;
  }

  private calcVisualDebt<T>(columns:ImageColumn<T>[]):number {
    let highestIndex = this.getHighestColIndex(columns);
    let highestValue = columns[highestIndex].sum;
    let sum = 0;
    for(let i=0;i<columns.length;i++)
      if(i!=highestIndex)
        sum += Math.abs(columns[i].sum-highestValue);

    return sum;
  }

  private calFutureVisualDebt(columns:ImageColumn<any>[],lowColIndex:number,colIndex:number,valueIndex:number):number {
    
    columns[lowColIndex].sum += columns[colIndex][valueIndex].value;
    columns[colIndex].sum -= columns[colIndex][valueIndex].value;

    let debt = this.calcVisualDebt(columns);

    columns[lowColIndex].sum -= columns[colIndex][valueIndex].value;
    columns[colIndex].sum += columns[colIndex][valueIndex].value;

    return debt;
  }

  private rerrange<T>(columns:ImageColumn<T>[],lowColIndex:number,colIndex:number,valueIndex:number) {
    columns[lowColIndex].push(columns[colIndex][valueIndex]);
    columns[colIndex].splice(valueIndex,1);

    columns[lowColIndex].update();
    columns[colIndex].update();

  }
}


export class ImageColumn<T>  extends Array<T> {


  private constructor(...args){
    super(...args);
  }

  static create<T>(): ImageColumn<T> {
        return Object.create(ImageColumn.prototype);
  }

  private _totalHeight = 0;
  public get sum(){
    return this._totalHeight||0;
  }

  public set sum(_sum) {
    this._totalHeight = _sum;
  }

  public update(){
    this._totalHeight = this.reduce((sum,curr)=>sum+=(<any>curr).value,0);
  }
} 
