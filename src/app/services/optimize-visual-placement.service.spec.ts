import { TestBed } from '@angular/core/testing';

import { OptimizeVisualPlacementService } from './optimize-visual-placement.service';

describe('OptimizeVisualPlacementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OptimizeVisualPlacementService = TestBed.get(OptimizeVisualPlacementService);
    expect(service).toBeTruthy();
  });
});
