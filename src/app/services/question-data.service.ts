import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"

@Injectable({
  providedIn: 'root'
})
export class QuestionDataService {

  constructor(private httpClient:HttpClient) { }
  private questions:Section[];

  async getQuestions():Promise<Section[]> {

  	if(this.questions)
  		return this.questions;

  	let sections:Section[] =  await this.httpClient.get<Section[]>("/assets/questions.json").toPromise();

  	for(let section of sections)
  		for(let question of section.questions)
  			for(let value in question.subSectionMatch)
  				question.subSections.push(Object.assign(new Section(),{...sections[question.subSectionMatch[value]],enabled:false}))

  	this.questions = sections;
  	return sections;
  }
}



export class Question {

	name:string;
	description:string;
	inputType:'radio'|'checkbox';
	required;
	options:{value:any,text:any,selected:boolean}[];
	values:any[];
	subSectionMatch = {};

	subSections = [] as Section[];
}


export class Section {
	id:number;
	enabled:boolean;
	title:string;
	questions:Question[];
}