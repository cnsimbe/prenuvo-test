import { ActivatedRouteSnapshot, DetachedRouteHandle, RouteReuseStrategy } from '@angular/router';
export class AppRouteStrategy implements RouteReuseStrategy {

  private handlers = {};
  shouldDetach(_route: ActivatedRouteSnapshot): boolean {
    return true;
  }

  shouldAttach(_route: ActivatedRouteSnapshot): boolean {
    let url = this.getURL(_route);
    return !!this.handlers[url]
  }

  store(_route: ActivatedRouteSnapshot, _detachedTree: DetachedRouteHandle): void {
    let url = this.getURL(_route);
    this.handlers[url] = _detachedTree;
  }

  retrieve(_route: ActivatedRouteSnapshot): DetachedRouteHandle | null {
    let url = this.getURL(_route);
    return this.handlers[url];
  }

  shouldReuseRoute(
    future: ActivatedRouteSnapshot,
    curr: ActivatedRouteSnapshot
  ): boolean {
    return future.routeConfig == curr.routeConfig;
  }

  private getURL(_route:ActivatedRouteSnapshot):string {
    let url = _route.pathFromRoot.map(a=>a.url.map(b=>b.toString()).filter(k=>!!k).join("/")).filter(k=>!!k).join("/")
    return url ? `/${url}` : url;
  }

}