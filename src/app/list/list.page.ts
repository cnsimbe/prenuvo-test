import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser'
import { LoadingController } from "@ionic/angular"
import { OptimizeVisualPlacementService, ImageColumn } from "../services/optimize-visual-placement.service"

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit, OnDestroy {
  
  @ViewChild('container') container:ElementRef;
  private imageResources:{src:string,value:number,safeSrc:SafeResourceUrl}[] = [];
  public displayGrid = [] as ImageColumn<{src:string,value:number,safeSrc:SafeResourceUrl}>[];
  public colWidth = '33%';

  constructor(private domSanitizer : DomSanitizer, private loadCtrl:LoadingController, private optimizer:OptimizeVisualPlacementService){}

  ngOnInit() {
    this.renderDisplay = this.renderDisplay.bind(this);
    window.addEventListener("resize",this.renderDisplay)
  }

  ngOnDestroy() {
    window.removeEventListener("resize",this.renderDisplay)
  }

  public async uploadFolder(inputElement:HTMLInputElement) {
    let fileList = inputElement.files as FileList;

    for(let i=0;i<this.imageResources.length;i++)
      URL.revokeObjectURL(this.imageResources[i].src); //revoke to save system resources and memory

    this.imageResources = [];

    let loader = await this.loadCtrl.create({message:'Loading images...'});
    loader.present();

    for(let i=0;i<fileList.length;i++)
    {
      let url = URL.createObjectURL(fileList.item(i))
      try
      {
        let img = await this.loadImage(url);
        this.imageResources.push({value: img.naturalHeight/img.naturalWidth, src:url ,safeSrc:this.domSanitizer.bypassSecurityTrustResourceUrl(url)})
      }catch(exp){}
    }

    inputElement.value = "";
    this.renderDisplay();

    await loader.dismiss()
  }


  private renderDisplay() {
    let contentWidth = (this.container.nativeElement as HTMLElement).getBoundingClientRect().width;
    let numCols = contentWidth < 575 ? 2 : 3;
    this.displayGrid = this.optimizer.optimizeView(this.imageResources,numCols);
    this.colWidth = `${100/numCols}%`
  }


  private async loadImage(URL):Promise<HTMLImageElement> {
    let image = document.createElement("img");
    image.src = URL;
    await new Promise((res,rej)=>{
      image.onload = $event=>res(image);
      image.onerror = error=>rej(error);
    })
    return image;
    
  }
}
